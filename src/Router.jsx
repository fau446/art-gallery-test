import React, { useState } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import MainPage from './MainPage';
import LoginPage from './components/LoginPage/LoginPage';
import SignupPage from './components/SignupPage/SignupPage';
import UserPage from './components/UserPage/UserPage';

function Router() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  function logIn() {
    setIsLoggedIn(true);
  }

  function logOut() {
    setIsLoggedIn(false);
  }

  const router = createBrowserRouter([
    {
      path: '/',
      element: <MainPage isLoggedIn={isLoggedIn} logOut={logOut} />,
    },
    {
      path: '/login',
      element: <LoginPage logIn={logIn} />,
    },
    {
      path: '/signup',
      element: <SignupPage logIn={logIn} />,
    },
    {
      path: '/userpage',
      element: <UserPage />,
    },
  ]);

  return <RouterProvider router={router} />;
}

export default Router;
