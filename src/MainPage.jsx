import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import Nav from './components/Nav/Nav';
import Hero from './components/Hero/Hero';
import Gallery from './components/Gallery/Gallery';
import data from './data';

function MainPage({ isLoggedIn, logOut }) {
  const newData = data.map((item) => ({
    ...item,
    id: uuidv4(),
  }));

  return (
    <>
      <Nav isLoggedIn={isLoggedIn} logOut={logOut} />
      <Hero />
      <Gallery data={newData} />
    </>
  );
}

export default MainPage;
