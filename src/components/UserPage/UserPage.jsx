import React from 'react';
import styles from './UserPage.module.css';
import Nav from '../Nav/Nav';

function UserPage() {
  return (
    <div>
      <Nav userPageNav />
      <div className={styles.banner}>
        <p>Banner</p>
      </div>
      <div className={styles.posts}>
        <h2>Your Posts</h2>
      </div>
    </div>
  );
}

export default UserPage;
