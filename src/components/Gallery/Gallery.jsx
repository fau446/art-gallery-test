import React, { useEffect, useState } from 'react';
import ItemFrame from '../ItemFrame/ItemFrame';
import Modal from '../modal/Modal';
import styles from './Gallery.module.css';
import { getArts } from '../../utils/api';

function Gallery() {
  const [isLoading, setIsLoading] = useState(true);
  const [artData, setArtData] = useState([]); // [artData, setArtData
  const [activeModelIndex, setActiveModelIndex] = useState(-1);

  useEffect(() => {
    getArts()
      .then((res) => {
        setArtData(res);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  function openModal(e) {
    setActiveModelIndex(Number(e.target.dataset.id));
  }

  function closeModal() {
    setActiveModelIndex(-1);
  }

  function handleKeyPress(e) {
    if (e.key === 'Enter' || e.key === ' ') {
      openModal();
    }
  }

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <div className={styles.gallery}>
      {artData.map((item, index) => (
        <div key={item.id}>
          <ItemFrame
            views={item.views}
            likes={item.likes}
            numOfComments={item.comments.length}
            imageURL={item.imageURL}
            openModal={openModal}
            handleKeyPress={handleKeyPress}
            id={index}
          />
          <Modal
            title={item.title}
            author={item.author}
            isOpen={activeModelIndex === index}
            views={item.views}
            likes={item.likes}
            comments={item.comments}
            imageURL={item.imageURL}
            closeModal={closeModal}
          />
        </div>
      ))}
    </div>
  );
}

export default Gallery;
