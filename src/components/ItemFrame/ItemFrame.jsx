import React from 'react';
import styles from './ItemFrame.module.css';

function ItemFrame({
  views, likes, numOfComments, imageURL, openModal, handleKeyPress, id,
}) {
  return (
    <div className={styles.item}>
      <img
        src={imageURL}
        alt="Post Pic"
        data-id={id}
        onClick={openModal}
        onKeyDown={handleKeyPress}
      />
      <div className={styles.bottom}>
        <p>
          Views:
          {' '}
          {views}
        </p>
        <p>
          Likes:
          {' '}
          {likes}
        </p>
        <p>
          Comments:
          {' '}
          {numOfComments}
        </p>
      </div>
    </div>
  );
}

export default ItemFrame;
