import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import styles from './Signup.module.css';
import { createAccount } from '../../utils/api';

function SignupPage({ logIn }) {
  const [createAccSuccess, setCreateAccSuccess] = useState(false);
  const navigate = useNavigate();

  async function submitForm(e) {
    e.preventDefault();
    const username = e.target[0].value;
    const password = e.target[1].value;
    createAccount({ username, password }).then((res) => res.status === 200);
    setCreateAccSuccess(true);
    await new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, 1000);
    });
    logIn();
    navigate('/');
  }

  return (
    <div className={styles.login}>
      {createAccSuccess && (
        <p className={styles.success}>Login Successful!</p>
      )}
      <h1>Create an Account</h1>
      <form onSubmit={submitForm}>
        <label htmlFor="username">
          Username:
          <input type="text" id="username" name="username" required />
        </label>
        <label htmlFor="password">
          Password:
          <input type="password" id="password" name="password" required />
        </label>
        <input className={styles.submit} type="submit" value="Sign Up" />
      </form>
      <Link to="/login"><input className={styles.createbtn} type="submit" value="Already Have an Account?" /></Link>
      <Link to="/"><input className={styles.homebtn} type="submit" value="Back to Homepage" /></Link>
    </div>
  );
}

export default SignupPage;
