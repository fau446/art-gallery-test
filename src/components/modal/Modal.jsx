import React from 'react';
import styles from './Modal.module.css';
import Comment from '../Comment/Comment';

function Modal({
  title, author, views, likes, comments, imageURL, isOpen, closeModal,
}) {
  if (!isOpen) {
    return null;
  }
  return (
    <div>
      <div className={styles.overlay} />
      <div className={styles.modal}>
        <div className={styles.top}>
          <button type="button" onClick={closeModal}>X</button>
        </div>
        <div className={styles.content}>
          <div className={styles.left}>
            <h2>{title}</h2>
            <p>
              By:
              {' '}
              {author}
            </p>
            <div className={styles.flex}>
              <p>
                Views:
                {' '}
                {views}
              </p>
              <p>
                Likes:
                {' '}
                {likes}
              </p>
            </div>
            <div className={styles.comments}>
              {comments.map((comment) => (
                <Comment
                  username={comment.username}
                  comment={comment.comment}
                  profilePic={comment.profilePic}
                />
              ))}
            </div>
          </div>
          <div className={styles.right}>
            <img src={imageURL} alt="Post" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Modal;
