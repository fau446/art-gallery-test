import React from 'react';
import styles from './Comment.module.css';

function Comment({ username, comment, profilePic }) {
  return (
    <div className={styles.comment}>
      <div className={styles.profilepic}>
        <img className={styles.avatar} src={profilePic} alt="Profile" />
      </div>
      <div className={styles.content}>
        <h3>{username}</h3>
        <p>{comment}</p>
      </div>
    </div>
  );
}

export default Comment;
