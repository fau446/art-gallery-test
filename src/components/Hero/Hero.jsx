import React from 'react';
import styles from './Hero.module.css';

function Hero() {
  // We can replace the background colour with a background image later.
  return (
    <div className={styles.hero}>
      <div className={styles.center}>
        <h1>Art Gallery</h1>
        <p>Art Gallery Description Here</p>
      </div>
    </div>
  );
}

export default Hero;
