/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import { Link } from 'react-router-dom';
import styles from './Nav.module.css';

function Nav({ isLoggedIn, userPageNav = false, logOut }) {
  if (userPageNav) {
    return (
      <nav className={styles.nav}>
        <Link to="/">Home Page</Link>
      </nav>
    );
  }

  return (
    <nav className={styles.nav}>
      {isLoggedIn ? (
        <>
          <p onClick={logOut}>Log out</p>
          <Link to="userpage">My Page</Link>
        </>
      ) : (
        <Link to="login">Login/Sign Up</Link>
      )}
    </nav>
  );
}

export default Nav;
