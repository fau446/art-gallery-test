import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import styles from './LoginPage.module.css';
import { login } from '../../utils/api';

function LoginPage({ logIn }) {
  const [showError, setShowError] = useState(false);
  const [loginSuccess, setLoginSuccess] = useState(false);
  const navigate = useNavigate();

  async function checkLogin(username, password) {
    return login({ username, password }).then(() => true).catch(() => false);
  }

  async function submitForm(e) {
    e.preventDefault();
    const loginStatus = await checkLogin(e.target[0].value, e.target[1].value);
    if (loginStatus) {
      // back to homepage with a delay
      console.log('login success');
      setShowError(false);
      setLoginSuccess(true);
      // display a modal that says success
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve();
        }, 1000);
      });
      logIn();
      navigate('/');
    } else {
      // display error
      console.log('login failed');
      setShowError(true);
    }
  }

  return (
    <div className={styles.login}>
      {showError && (
        <p className={styles.error}>Username or password is incorrect. Please try again.</p>
      )}
      {loginSuccess && (
        <p className={styles.success}>Login Successful!</p>
      )}
      <h1>Log in to Art Gallery</h1>
      <form onSubmit={submitForm}>
        <label htmlFor="username">
          Username:
          <input type="text" id="username" name="username" required />
        </label>
        <label htmlFor="password">
          Password:
          <input type="password" id="password" name="password" required />
        </label>
        <input className={styles.submit} type="submit" value="Log in" />
      </form>
      <Link to="/signup"><input className={styles.createbtn} type="submit" value="Create an Account" /></Link>
      <Link to="/"><input className={styles.homebtn} type="submit" value="Back to Homepage" /></Link>
    </div>
  );
}

export default LoginPage;
