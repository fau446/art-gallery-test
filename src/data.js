const data = [{
  title: 'Moonlight',
  author: 'James Douglas',
  views: '50',
  likes: '40',
  imageURL: 'https://picsum.photos/1920/1080',
  comments: [{ username: 'Freddie Mays', comment: 'Nice photo!', profilepic: 'https://picsum.photos/500/500' },
    { username: 'Bill Polk', comment: 'That looks great! Good job!', profilepic: 'https://picsum.photos/500/500' }, { username: 'Dave Norton', comment: 'Wow! You have to show me how to take great pictures like this!', profilepic: 'https://picsum.photos/500/500' }, { username: 'Freddie Mays', comment: 'Nice photo!', profilepic: 'https://picsum.photos/500/500' }, { username: 'Bill Polk', comment: 'That looks great! Good job!', profilepic: 'https://picsum.photos/500/500' }, { username: 'Dave Norton', comment: 'Wow! You have to show me how to take great pictures like this!', profilepic: 'https://picsum.photos/500/500' }],
}, {
  title: 'Sunrise', author: 'Douglas Douglas', views: '10', likes: '2', imageURL: 'https://picsum.photos/1920/2880', comments: [{ username: 'Ted Ned', comment: 'Love it!', profilepic: 'https://picsum.photos/500/500' }],
}, {
  title: 'Cup', author: 'Peter Parker', views: '20', likes: '1', imageURL: 'https://picsum.photos/1920/1280', comments: [{ username: 'Bill Polk', comment: 'Good photo!', profilepic: 'https://picsum.photos/500/500' }],
}, {
  title: 'Laptop', author: 'Pete Davidson', views: '0', likes: '0', imageURL: 'https://picsum.photos/1920/2400', comments: [],
}];

export default data;
