export const BASE_URL = 'https://art-gallery-sfu.onrender.com';

export const checkResponse = (res) => (res.ok ? res.json() : res.json()
  .then((err) => Promise.reject(err)));

export const getArts = async () => checkResponse(await fetch(`${BASE_URL}/arts`));

export const getArtById = async (id) => checkResponse(await fetch(`${BASE_URL}/arts/${id}`));

export const addArt = async (art) => {
  const res = await fetch(`${BASE_URL}/arts`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(art),
  });
  return checkResponse(res);
};

export const removeArt = async (id) => {
  const res = await fetch(`${BASE_URL}/arts/${id}`, {
    method: 'DELETE',
  });
  return checkResponse(res);
};

export const getComments = async (id) => checkResponse(await fetch(`${BASE_URL}/arts/${id}/comments`));

export const addComment = async (id, comment) => {
  const res = await fetch(`${BASE_URL}/arts/${id}/comments`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(comment),
  });
  return checkResponse(res);
};

export const removeComment = async (id, commentId) => {
  const res = await fetch(`${BASE_URL}/arts/${id}/comments/${commentId}`, {
    method: 'DELETE',
  });
  return checkResponse(res);
};

export const createAccount = async (account) => {
  const res = await fetch(`${BASE_URL}/create-account`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(account),
  });
  return checkResponse(res);
};

export const login = async (account) => {
  const res = await fetch(`${BASE_URL}/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(account),
  });
  return checkResponse(res);
};
